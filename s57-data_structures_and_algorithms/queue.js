let collection = [];

module.exports.print = () => {
    return collection
};

module.exports.enqueue = (element) => {
    collection.push(element);
    return collection;
};

module.exports.dequeue = () => {
    if(collection.length > 0) {
        collection.shift();
    }
    return collection;
};

module.exports.peek = () => {
    return collection[collection.length -1];
};

module.exports.size = () => {
    return collection.length
};

module.exports.isEmpty = () => {
    return collection.length == 0;
};

module.exports.front = () => {
    if(collection.length > 0) {
        return collection[0];
    }else{
        return ("The Queue is empty")
    }
};
